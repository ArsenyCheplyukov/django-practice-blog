## **Steps of creating Django app:**
- #### **Building virtual enviornment with poetry:**
    ##### 1. Use command ```poetry init```
    ##### 2. Provide all dependences that will be asked in this step
    ##### 3. After creating activate poetry by typing: ```poetry shell```
    ##### 4. Provide all dependences (if you not using them in initialization), in this case that only one dependency: ```poetry add Django```
    ##### 5. Install them by typing: ```poetry install```

- #### **Build Django application**
    1. ##### To create djangp app type ```django-admin startproject {project_name}```. In our case project_name is blog
    2. ##### After creating django aoplication init django page by moving to project folder: ```cd blog```, as blog is our's project name and start page with: ```python manage.py startapp {page_name}```, in my case page_name is 'blog_main'

- #### **Make database in models.py:**
    1. ##### Determine the structure of this project and create class that represents your table in datatbase (name of class is tablae name)
    2. ##### Inherits from models.Model
    3. ##### Wrtie used fields with appropriate fields
    4. ##### If needed, provide distribuition of data by using Meta class: by attribute ordering where you privide list of strings of needed columns, if invered add - as first symbol in it
    5. ##### Here you can use indexes that you init in Meta class, in indexes parameter: here you also provide list of strings like in orderins attribute, but you call models.inddex method and provide that list in fields argument
    6. ##### If needed add foreign keys with models.ForeignKey and write database that foreign to this in start. Also provide as argument on_delete parameter, in my case is models.CASCADE, and related_name as name of field in foreign database
    7. ##### If you have done creating a new model, make migrations and apply them by typing: ```python namage.py makemigrations blog_main``` and ```python manage.py migrate```

- #### **Edit admin.py:**
    1. ##### Create super user by typing ```python manage.py createsuperuser``` and write all parametes that asking in terminal
    2. ##### To chack is this working type: ```python manage.py runserver``` and move to ```http://127.0.0.1:8000/admin/```
    3. ##### Import models from .models (best pracitce is not importing all, but only needed models)
    4. ##### Register viewed in admin url models by add admin.site.register({model_name}), in my case model_name is called Post
    5. ##### If needed some unique view in admin panel we should use class with decorator ```@admin.register(Post)``` - name of group. In my case this class is PostAdmin. Don't forget to inherit from admin.ModelAdmin. Here we can add fields that are showing by using list display with list typing

- #### Edit views.py to add view:
  1. ##### Add view for all posts. There we get all objects from our model that represents one table in database. So inherit here we write in functional style: create some function with one of the parameters response and pass it render function that we get from django.shortcuts: render(request, {refference to view template, named paramaters in dictionary})
  2. ##### Try feature of get_object_or_404 also from shortcuts here we pass model as first parameter, then named fields of this model and pass it in render function to return. If there any arguments
- #### Modify urls.py
  1. ##### Add our application in main urls.py with path function, first parameter of what is url ref, in our case "blog/" then include function with first parameter "blog.urls" and last paramater namespace="blog"
  2. ##### Add urls in our application: with path in urlpatterns list: first is our main-page url, then function, then name, like this: ```path('', views.post_list, name='post_list')```. If any parameter appears hold it in {<type:>} in example: <int:id>
- #### Add views templates:
  *Here is many approaches to keep templates in: folder with code for models or in templates folder in main folder, here i keep templates in main folder*
    1. ##### Create hiererchy of folders:
        - templates
            - blog
                - post
                    - detail.html
                    - list.html
                - base.html
    2. ##### Fill base.html with code, here some tips: add {% load static %} at top, in html tag add head tag and pass ```<link href="{% static "css/blog.css" %}" rel="stylesheet">```
    3. ##### Add detail.html code: extend blocks ```{% extends "blog/base.html" %}```, add ```|linebreaks``` to parameters
    4. ##### Add list.html code: also extend blocks ```{% extends "blog/base.html" %}```, add ```|truncatewords:30|linebreaks``` and ```{% for post in posts %}``` for itterations
- #### Fix absolute url:
    1. ##### Add get_absolute_url method in model where this method needed. To make this you need to return reverse function (from django.urls import reverse) first argument is view refference and second is parameters that you need to pass in this view to make it work
    2. ##### So by now, you can use get_absolute_url as parameter in view connected with this model
- #### Fix unique slug for some date:
    1. ##### Add in needed field in model, in our case 'slug' parameter unique_for_date='publish'
   
- #### Create a canonical URL for model:
    1. ##### Create get_absolute_url in models.py in your model. Here we return reverse method (djnago.urls), like this: ```reverse('blog:post_detail', args=[self.id])```
    2. ##### Now we can use post.get_absolute_url in view template
- #### Change urls to date and post title, like: ```2023/11/9/some-title```:
- #### Change fields in model for unique field for some date:
    1. ##### Add uniquness field in field of model like this: unique_for_date='publish'
    2. ##### Make migration: ```python manage.py migrate```
    3. ##### Change url path in urls.py like this: ```path('<int:year>/<int:month>/<int:day>/<slug:post>/', views.post_detail, name='post_detail')```
    4. ##### Change views.py like this: ```post = get_object_or_404(Post, status=Post.Status.PUBLISHED, slug=post, publish__year=year, publish__month=month, publish__day=day)```
    5. ##### Change get_absolute_url to new return: ```reverse('blog:post_detail', args=[self.publish.year, self.publish.month, self.publish.day, self.slug])```
- #### Add pagination (page divide):
    1. ##### Import paginator to view: from django.core.paginator import Paginator 
    2. ##### Remake paginator in view: ```paginator = Paginator(post_list, 3)```      ```page_number = request.GET.get('page', 1)```    ```posts = paginator.page(page_number)```
    3. ##### Include paginator in view template: ```{% include "pagination.html" with page=posts %}```
    4. ##### Try to avoid errors by applying principle: BAFP: ```from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger``` and work with exceptions that appears here
    5. ##### But this all can be replaced by using class view like this: ```class PostListView(ListView):```      ```queryset = Post.published.all()```       ```context_object_name = 'posts'```         ```paginate_by = 3```            ```template_name = 'blog/post/list.html'```
    6. ##### Also don't forget to replace view url in urls.py, when we are using class views: ```path('', views.PostListView.as_view(), name='post_list')```
    7. ##### Also we should replace including of paginator in view template: ```{% include "pagination.html" with page=page_obj %}```
- #### Create form for passing parameters in it:
    1. ##### Create forms.py and add import in it: ```from django import forms```
    2. ##### Create class form to add email for appying sharing post (like model class): ```class EmailPostForm(forms.Form):```    ```name = forms.CharField(max_length=25)```    ```email = forms.EmailField()```    ```to = forms.EmailField()```    ```comments = forms.CharField(required=False, widget=forms.Textarea)```
    3. ##### 


* ##### Important:
  * Types of fields in models
  * Meta class in models
  * Classes for fields
  * Admin class filters
  * Admin class all fields
  * templates for views
  * paginator
  * Sitemap (how it's used in browser search and etc.)
  * Forms
  * Email work