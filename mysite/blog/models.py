"""Database setup"""
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from taggit.managers import TaggableManager


class PublishedManager(models.Manager):
    """creates manager for optimizing long queries"""
    def get_queryset(self):
        return super().get_queryset().filter(status=Post.Status.PUBLISHED)


# Create your models here.
class Post(models.Model):
    """Model for keeping posts in blog info"""
    # create class for point data hierarchy for status
    class Status(models.TextChoices):
        """
        Status of post (published or draft with there brief names)
        """
        DRAFT = 'DF', 'DRAFT'
        PUBLISHED = 'PB', 'Published'

    # name of post
    title = models.CharField(max_length=256)
    # unique link for post in url (should be indexed, as it's slug field)
    slug = models.SlugField(max_length=256, unique_for_date='publish')
    # key for connect user to this database (one user to many posts)
    # should indexed as it's foreign key
    author = models.ForeignKey(User,
                               on_delete=models.CASCADE,
                               related_name='blog_posts')

    # text of post
    body = models.TextField()
    # use method initialized by default, not clalled
    publish = models.DateTimeField(default=timezone.now)
    # add date when creating
    created = models.DateTimeField(auto_now_add=True)
    # add date when using update field
    updated = models.DateTimeField(auto_now=True)
    # status checkout
    status = models.CharField(max_length=2,
                              choices=Status.choices,
                              default=Status.DRAFT)

    # manager by default
    objects = models.Manager()
    # manager for published posts
    published = PublishedManager()
    # add tags for posts
    tags = TaggableManager()

    class Meta:
        """Class to modify model's behavior"""
        # point attributes order that showing from ORM
        ordering = ["-publish"]
        indexes = [
            models.Index(fields=["-publish"]),
        ]

    def __str__(self) -> str:
        # TO DO: fix type shift
        return str(self.title)
    
    def get_absolute_url(self):
        """returns absolute url address from given one"""
        return reverse('blog:post_detail',
                       args=[self.publish.year,
                             self.publish.month,
                             self.publish.day,
                             self.slug])


class Comment(models.Model):
    """Comment database, for adding comments to post"""
    post = models.ForeignKey(Post,
                             on_delete=models.CASCADE,
                             related_name='comments')
    name = models.CharField(max_length=80)
    email = models.EmailField()
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)

    class Meta:
        """modifying behavior (order, index fields)"""
        ordering = ['created']
        indexes = [
            models.Index(fields=['created'])
        ]

    def __str__(self):
        return f'Comment by {self.name} on {self.post}'
