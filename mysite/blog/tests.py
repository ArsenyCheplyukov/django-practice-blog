"""Tests that pulls automatically when ```python manage.py tests```"""
from django.test import TestCase

from django.contrib.admin.sites import AdminSite
from django.contrib.auth.models import User
from django.utils import timezone
from .admin import AdminPost
from .models import Post


# Create your tests here.
class PostAdminTest(TestCase):
    """Test case for the Post admin panel."""

    def setUp(self):
        """Set up the environment for tests."""
        self.site = AdminSite()
        self.post_admin = AdminPost(Post, self.site)

    def test_admin_list_display(self):
        """Test the list display in Post admin."""
        list_display = self.post_admin.get_list_display(None)
        self.assertEqual(list_display, ['title', 'slug', 'author', 'publish', 'status'])

    def test_admin_list_filter(self):
        """Test the list filter in Post admin."""
        list_filter = self.post_admin.get_list_filter(None)
        self.assertEqual(list_filter, ['status', 'created', 'publish', 'author'])

    def test_admin_search_fields(self):
        """Test the search fields in Post admin."""
        search_fields = self.post_admin.get_search_fields(None)
        self.assertEqual(search_fields, ['title', 'body'])

    def test_admin_prepopulated_fields(self):
        """Test the prepopulated fields in Post admin."""
        prepopulated_fields = self.post_admin.get_prepopulated_fields(None)
        self.assertEqual(prepopulated_fields, {'slug': ('title',)})

    def test_admin_raw_id_fields(self):
        """Test if the 'author' field is a raw ID field in Post admin."""
        raw_id_fields = self.post_admin.raw_id_fields
        expected_raw_id_fields = ['author']
        self.assertEqual(raw_id_fields, expected_raw_id_fields,
                         f"Expected raw ID fields: {expected_raw_id_fields}"
                         f"Actual raw ID fields: {raw_id_fields}")

    def test_admin_date_hierarchy(self):
        """Test the date hierarchy in Post admin."""
        date_hierarchy = self.post_admin.date_hierarchy
        self.assertEqual(date_hierarchy, 'publish')

    def test_admin_ordering(self):
        """Test the ordering in Post admin."""
        ordering = self.post_admin.get_ordering(None)
        self.assertEqual(ordering, ['status', 'publish'])


class PostModelTest(TestCase):
    """Test case for the Post model."""

    def setUp(self):
        """Set up the environment for tests."""
        self.user = User.objects.create_user(username='testuser', password='12345')

    def tearDown(self):
        """Clean up test data after each test method."""
        User.objects.filter(username='testuser').delete()
        Post.objects.filter(title='Test Post').delete()

    def test_post_str_representation(self):
        """Test the string representation of Post."""
        post = Post.objects.create(
            title='Test Post',
            slug='test-post',
            author=self.user,
            body='This is a test post body',
            publish=timezone.now(),
            status=Post.Status.DRAFT
        )
        self.assertEqual(str(post), 'Test Post')

    def test_post_creation(self):
        """Test the creation of a Post instance."""
        post = Post.objects.create(
            title='Test Post',
            slug='test-post',
            author=self.user,
            body='This is a test post body',
            publish=timezone.now(),
            status=Post.Status.DRAFT
        )
        self.assertTrue(isinstance(post, Post))
