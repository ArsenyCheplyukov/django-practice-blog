"""Admin panel set up"""
from django.contrib import admin
from .models import Post, Comment


# Register your models here.
@admin.register(Post)
class AdminPost(admin.ModelAdmin):
    """Create custom post view in admin panel"""
    # point fields that are showing and could be entered
    list_display = ["title", "slug", "author", "publish", "status"]
    # write list at right part to apply filter to
    list_filter = ["status", "created", "publish", "author"]
    # fields for searching in
    search_fields = ["title", "body"]
    # auto fill slug with data of title
    prepopulated_fields = {"slug": ("title",)}
    # display a ForeignKey as a text input rather than a select box
    raw_id_fields = ["author"]
    # use date from publish field
    date_hierarchy = "publish"
    # show first by status, then by publish in equal status
    ordering = ["status", "publish"]


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    """Custom admin panel interface for comments"""
    list_display = ['name', 'email', 'post', 'created', 'active']
    list_filter = ['active', 'created', 'updated']
    search_fields = ['name', 'email', 'body']
