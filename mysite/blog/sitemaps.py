from django.contrib.sitemaps import Sitemap
from .models import Post


class PostSitemap(Sitemap):
    """Create sitemap for blog"""
    # define change frquency
    changefreq = 'weekly'
    # percentage of new data in site
    priority = 0.9

    def items(self):
        """Get objects that needed to be passed into site map"""
        return Post.published.all()

    def lastmod(self, obj):
        """Returns time of last modification for any obj in items"""
        return obj.updated
