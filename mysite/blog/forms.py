from django import forms
from .models import Comment


class EmailPostForm(forms.Form):
    """Form to send post via email"""
    name = forms.CharField(max_length=25)
    email = forms.EmailField()
    to = forms.EmailField()
    comments = forms.CharField(required=False,
                               widget=forms.Textarea)


class CommentForm(forms.ModelForm):
    """Form for entering comment"""
    class Meta:
        """modificate model behavior"""
        model = Comment
        fields = ['name', 'email', 'body']


class SearchForm(forms.Form):
    """Form allowing enter text search"""
    query = forms.CharField()
